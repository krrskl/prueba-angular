// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  ts: 1,
  publicKey: "0a59f9a5608a7bec143e5f6f95ec4310",
  hash: "316292d95a9003f8783b62d003cb253c",
  api: "https://gateway.marvel.com:443/v1/public",
  nameStore: "prueba_anguar_ruben_marvel_app_users"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

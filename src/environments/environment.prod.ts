export const environment = {
  production: true,
  ts: 1,
  publicKey: "0a59f9a5608a7bec143e5f6f95ec4310",
  hash: "316292d95a9003f8783b62d003cb253c",
  api: "https://gateway.marvel.com:443/v1/public",
  nameStore: "prueba_anguar_ruben_marvel_app_users"
};

import * as fromAuth from "./services/auth/auth.reducer";
import * as fromUI from "./services/ui/ui.reducer";
import * as fromHEROE from "./services/heroes/heroe.reducer";
import { ActionReducerMap } from "@ngrx/store";

export interface AppState {
  auth: fromAuth.AuthState;
  ui: fromUI.UIState;
  heroes: fromHEROE.HeroeState;
}

export const appReducers: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  ui: fromUI.uiReducer,
  heroes: fromHEROE.HeroeReducer
};

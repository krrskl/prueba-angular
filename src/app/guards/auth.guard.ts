import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "../services/auth/auth.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate() {
    this.authService.getUser().then(({ user }) => {
      if (!user) {
        this.router.navigateByUrl("/login");
        return false;
      }
      this.authService.setUser(user);
    });
    return true;
  }
}

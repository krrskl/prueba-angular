import { User } from "./user.model";

export interface Storage {
  user:  User;
  users: User[];
}

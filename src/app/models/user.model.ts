import { Heroe } from "./heroe.model";

export interface User {
  email: number;
  name: string;
  password: string;
  photo: string;
  favorites: Array<Heroe>;
}

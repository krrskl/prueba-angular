import { Heroe } from "./heroe.model";

export interface HeroesResponse {
  data: {
    results: Heroe[];
  };
  code: number;
}

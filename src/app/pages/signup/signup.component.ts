import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { AppState } from "../../app.reducers";
import { Store } from "@ngrx/store";
import { EnableLoadingAction } from "src/app/services/ui/ui.actions";
import { AuthService } from "src/app/services/auth/auth.service";
import { name, email, password } from "../../utils/forms";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit {
  public form: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private AuthService: AuthService,
    private store: Store<AppState>
  ) {
    this.form = this.formBuilder.group({
      email: email(),
      password: password(),
      name: name()
    });
  }

  ngOnInit() {}

  signup($event) {
    $event.preventDefault();
    this.store.dispatch(new EnableLoadingAction());
    this.AuthService.signup(
      this.form.value.email,
      this.form.value.password,
      this.form.value.name
    );
  }
}

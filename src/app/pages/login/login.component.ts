import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { email, password } from "src/app/utils/forms";
import { AppState } from "../../app.reducers";
import { Store } from "@ngrx/store";
import { EnableLoadingAction } from "src/app/services/ui/ui.actions";
import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private AuthService: AuthService,
    private store: Store<AppState>
  ) {
    this.form = this.formBuilder.group({
      email: email(),
      password: password()
    });
  }

  ngOnInit() {}

  login($event) {
    $event.preventDefault();
    this.store.dispatch(new EnableLoadingAction());
    this.AuthService.login(this.form.value.email, this.form.value.password);
  }
}

import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { defaultField } from "src/app/utils/forms";
import { HeroesService } from "../../services/heroes/heroes.service";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducers";
import { SetHeroesAction } from "src/app/services/heroes/heroe.actions";
import { Heroe } from "../../models/heroe.model";
import { User } from "../../models/user.model";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  tabActive: boolean = true;
  public form: FormGroup;
  public heroes: Heroe[];
  public user: User;
  public heroesToShow: Heroe[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private heroeService: HeroesService,
    private store: Store<AppState>
  ) {
    this.form = this.formBuilder.group({
      query: defaultField()
    });
  }

  ngOnInit() {
    this.store.select("heroes").subscribe(({ heroes, heroesToShow }) => {
      if (heroes) this.heroes = heroes;
      if (heroesToShow)
        this.heroesToShow = heroesToShow;
    });

    this.store.select("auth").subscribe(({ user }) => {
      if (user) this.user = user;
    });
  }

  changeTab(state) {
    this.tabActive = state;
  }

  search($event) {
    $event.preventDefault();
    this.heroeService.findHeroe(this.form.value.query);
  }
}

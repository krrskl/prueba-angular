import { Action } from "@ngrx/store";

export enum UIActionsTypes {
  ENABLE_LOADING = "[UI Loading] Loading...",
  DISABLE_LOADING = "[UI Loading] End of loading...",
  TOGGLE_MODAL = "[UI TOGGLE MODAL] TOGGLE MODAL"
}

export class EnableLoadingAction implements Action {
  readonly type = UIActionsTypes.ENABLE_LOADING;
}

export class DisableLoadingAction implements Action {
  readonly type = UIActionsTypes.DISABLE_LOADING;
}

export class ToggleModalAction implements Action {
  readonly type = UIActionsTypes.TOGGLE_MODAL;
}

export type actions =
  | EnableLoadingAction
  | DisableLoadingAction
  | ToggleModalAction;

import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class UiService {
  constructor(private toast: ToastrService) {}

  /**
   * 
   * @param message Message to show in the toast
   * @param title Title for the toast
   * @param type Type of toast to show
   */
  async presentToast(message, title = "", type) {
    switch (type) {
      case "success":
        this.toast.success(message, title);
        break;
      case "warning":
        this.toast.warning(message, title);
        break;
      case "error":
        this.toast.error(message, title);
        break;
      case "info":
        this.toast.info(message, title);
        break;
      default:
        break;
    }
  }
}

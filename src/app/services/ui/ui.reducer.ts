import * as fromUI from "./ui.actions";

export interface UIState {
  isLoading: boolean;
  showModal: boolean;
}

const initState: UIState = {
  isLoading: false,
  showModal: false
};

export function uiReducer(state = initState, action: fromUI.actions): UIState {
  switch (action.type) {
    case fromUI.UIActionsTypes.ENABLE_LOADING:
      return { ...state, isLoading: true };

    case fromUI.UIActionsTypes.DISABLE_LOADING:
      return { ...state, isLoading: false };

    case fromUI.UIActionsTypes.TOGGLE_MODAL:
      return { ...state, showModal: !state.showModal };

    default:
      return state;
  }
}

import { Injectable } from "@angular/core";
import { StorageService } from "../storage/storage.service";
import { AppState } from "../../app.reducers";
import { Store } from "@ngrx/store";
import { DisableLoadingAction } from "../ui/ui.actions";
import { UiService } from "../ui/ui.service";
import { Router } from "@angular/router";
import {
  SetUserAction,
  AddHeroeToFavorite,
  RemoveHeroeOfFavorite
} from "./auth.actions";
import { User } from "../../models/user.model";
import { Heroe } from "src/app/models/heroe.model";
import { Storage } from "../../models/storage.model";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    private storageService: StorageService,
    private store: Store<AppState>,
    public ui: UiService,
    public router: Router
  ) {}

  /**
   *
   * @param email User email
   * @param password User password
   */
  public login(email, password) {
    this.storageService.getItem().then((storage: Storage) => {
      let canAuth = false;
      if (storage && storage.users) {
        for (let index = 0; index < storage.users.length; index++) {
          const user: User = storage.users[index];
          if (user.email == email && user.password == password) {
            canAuth = true;
            storage.user = user;
            this.storageService.setItem(storage).then(_ => {
              this.ui.presentToast(
                `Bienvenido ${user.name}`,
                "Inicio de sesión",
                "success"
              );
              this.store.dispatch(new SetUserAction(user));
              this.router.navigateByUrl("/home");
              this.store.dispatch(new DisableLoadingAction());
            });
            break;
          }
        }
      }

      if (!canAuth) {
        this.ui.presentToast(
          "Usuario o contraseña incorrectos.",
          "Inicio de sesión",
          "error"
        );
        this.store.dispatch(new DisableLoadingAction());
      }
    });
  }

  /**
   *
   * @param email User email
   * @param password User password
   * @param name Name of user
   */
  public signup(email, password, name) {
    this.storageService.getItem().then((storage: Storage) => {
      let canStore = true;
      if (storage && storage.users.length != 0) {
        /**
         * Validation of a unique email
         */
        for (let index = 0; index < storage.users.length; index++) {
          const user = storage.users[index];
          if (user.email == email) {
            this.ui.presentToast(
              "Este correo ya se encuentra en uso.",
              "Crear cuenta",
              "error"
            );
            canStore = false;
            break;
          }
        }
        if (canStore)
          storage.users = [
            ...storage.users,
            { email, password, name, favorites: [], photo: "" }
          ];
      } else {
        storage = {
          users: [{ email, password, name, favorites: [], photo: "" }],
          user: null
        };
      }

      if (canStore)
        this.storageService.setItem(storage).then(_ => {
          this.store.dispatch(new DisableLoadingAction());
          this.ui.presentToast(
            "Cuenta creada correctamente.",
            "Crear cuenta",
            "success"
          );
          this.router.navigateByUrl("/login");
        });
      else this.store.dispatch(new DisableLoadingAction());
    });
  }

  public getUser() {
    return this.storageService.getItem();
  }

  /**
   *
   * @param user Current user to set in the store
   */
  public setUser(user) {
    this.store.dispatch(new SetUserAction(user));
  }

  /**
   *
   * @param heroe Heroe to add as favorite in authenticated user
   */
  public addFavorite(heroe: Heroe) {
    this.updateUser();
    this.storageService.getItem().then((storage: Storage) => {
      const { user } = storage;
      let result = user.favorites.find(
        (element: Heroe) => element.id == heroe.id
      );
      if (!result) {
        this.ui.presentToast(
          "Héroe agregado a favoritos.",
          "Agregar a favoritos",
          "success"
        );
        this.store.dispatch(new AddHeroeToFavorite(heroe));
      } else {
        this.ui.presentToast(
          "Este héroe ya se encuentra agregado a favoritos.",
          "Agregar a favoritos",
          "warning"
        );
      }
    });
  }

  updateUser() {
    this.store.select("auth").subscribe(({ user }) => {
      this.storageService.getItem().then((storage: Storage) => {
        storage.user = user;
        this.storageService.setItem(storage);
      });
    });
  }

  /**
   *
   * @param heroe Heroe to remove of favorite in the authenticated user
   */
  public removeFavorite(heroe: Heroe) {
    this.updateUser();
    this.store.dispatch(new RemoveHeroeOfFavorite(heroe));
  }

  public logout() {
    this.storageService.deleteItem("user").then(_ => {
      this.store.dispatch(new SetUserAction(null));
      this.router.navigateByUrl("/login");
    });
  }
}

import { User } from "../../models/user.model";
import * as fromAUTH from "./auth.actions";
import { Heroe } from "src/app/models/heroe.model";

export interface AuthState {
  user?: User;
}

const initState: AuthState = {
  user: null
};

export function authReducer(
  state = initState,
  action: fromAUTH.actions
): AuthState {
  switch (action.type) {
    case fromAUTH.AuthActionTypes.SET_USER:
      return {
        user: { ...action.user }
      };

    case fromAUTH.AuthActionTypes.ADD_HEROE_TO_FAVORITE:
      return {
        user: {
          ...state.user,
          favorites: [...state.user.favorites, { ...action.heroe }]
        }
      };

    case fromAUTH.AuthActionTypes.REMOVE_HEROE_OF_FAVORITE:
      return {
        user: {
          ...state.user,
          favorites: state.user.favorites.filter(
            (heroe: Heroe) => heroe !== action.heroe
          )
        }
      };

    default:
      return state;
  }
}

import { Action } from "@ngrx/store";
import { User } from "../../models/user.model";
import { Heroe } from "../../models/heroe.model";

export enum AuthActionTypes {
  SET_USER = "[AUTH] LOGIN USER",
  ADD_HEROE_TO_FAVORITE = "[USER] AD HEROE TO FAVORITE",
  REMOVE_HEROE_OF_FAVORITE = "[USER] REMOVE HEROE OF FAVORITE"
}

export class SetUserAction implements Action {
  readonly type = AuthActionTypes.SET_USER;
  constructor(public user: User) {}
}

export class AddHeroeToFavorite implements Action {
  readonly type = AuthActionTypes.ADD_HEROE_TO_FAVORITE;
  constructor(public heroe: Heroe) {}
}

export class RemoveHeroeOfFavorite implements Action {
  readonly type = AuthActionTypes.REMOVE_HEROE_OF_FAVORITE;
  constructor(public heroe: Heroe) {}
}

export type actions = SetUserAction | AddHeroeToFavorite | RemoveHeroeOfFavorite;

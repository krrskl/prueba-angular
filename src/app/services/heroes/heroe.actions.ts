import { Action } from "@ngrx/store";
import { Heroe } from "src/app/models/heroe.model";

export enum HeroeActionsTypes {
  SET_HEROES = "[HEROES] SET HEROES",
  ADD_HEROE_TO_SHOW = "[HEROES] ADD HEROE TO SHOW IN ASIDE LAYOUT",
  SET_HEROE = "[HEROES] SET HEROE TO DETAILS",
  REMOVE_HEROE = "[HEROES] REMOVE HEROE"
}

export class SetHeroesAction implements Action {
  readonly type = HeroeActionsTypes.SET_HEROES;
  constructor(public heroes: Heroe[]) {}
}

export class AddHeroeToShowAction implements Action {
  readonly type = HeroeActionsTypes.ADD_HEROE_TO_SHOW;
  constructor(public heroe: Heroe) {}
}

export class SetHeroeAction implements Action {
  readonly type = HeroeActionsTypes.SET_HEROE;
  constructor(public heroe: Heroe) {}
}

export class RemoveHeroeAction implements Action {
  readonly type = HeroeActionsTypes.REMOVE_HEROE;
  constructor(public heroe: Heroe) {}
}

export type actions =
  | SetHeroesAction
  | AddHeroeToShowAction
  | SetHeroeAction
  | RemoveHeroeAction;

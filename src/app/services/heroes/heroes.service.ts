import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducers";
import {
  DisableLoadingAction,
  EnableLoadingAction,
  ToggleModalAction
} from "../ui/ui.actions";
import { HeroesResponse } from "../../models/heroesResponse.model";
import {
  SetHeroesAction,
  AddHeroeToShowAction,
  SetHeroeAction,
  RemoveHeroeAction
} from "./heroe.actions";
import { Heroe } from "../../models/heroe.model";
@Injectable({
  providedIn: "root"
})
export class HeroesService {
  constructor(private http: HttpClient, private store: Store<AppState>) {}

  /**
   *
   * @param query Value to search
   */
  public findHeroe(query) {
    this.store.dispatch(new EnableLoadingAction());
    this.http
      .get<HeroesResponse>(
        `${environment.api}/characters?nameStartsWith=${query}&apikey=${environment.publicKey}&hash=${environment.hash}&ts=${environment.ts}`
      )
      .subscribe(response => {
        console.log(response.data.results);
        this.store.dispatch(new SetHeroesAction(response.data.results));
        this.store.dispatch(new DisableLoadingAction());
      });
  }

  /**
   *
   * @param heroe Object of type Heroe
   */
  public addHeroeToShow(heroe: Heroe) {
    this.store.dispatch(new AddHeroeToShowAction(heroe));
  }

  /**
   *
   * @param heroe Object of type Heroe
   */
  public setHeroeToDetails(heroe: Heroe) {
    this.store.dispatch(new SetHeroeAction(heroe));
    this.store.dispatch(new ToggleModalAction());
  }

  /**
   *
   * @param heroe Heroe objet to remove of aside layout
   */
  public removeHeroe(heroe: Heroe) {
    this.store.dispatch(new RemoveHeroeAction(heroe));
  }
}

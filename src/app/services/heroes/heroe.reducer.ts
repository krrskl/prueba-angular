import * as fromHEROE from "./heroe.actions";
import { Heroe } from "../../models/heroe.model";

export interface HeroeState {
  heroes: Heroe[];
  heroesToShow: Heroe[];
  heroe: Heroe;
}

const initState: HeroeState = {
  heroes: [],
  heroesToShow: [],
  heroe: null
};

export function HeroeReducer(
  state = initState,
  action: fromHEROE.actions
): HeroeState {
  switch (action.type) {
    case fromHEROE.HeroeActionsTypes.SET_HEROES:
      return {
        ...state,
        heroes: [...action.heroes]
      };

    case fromHEROE.HeroeActionsTypes.ADD_HEROE_TO_SHOW:
      return {
        ...state,
        heroesToShow: [...state.heroesToShow, { ...action.heroe }]
      };
    case fromHEROE.HeroeActionsTypes.SET_HEROE:
      return {
        ...state,
        heroe: { ...action.heroe }
      };

    case fromHEROE.HeroeActionsTypes.REMOVE_HEROE:
      return {
        ...state,
        heroesToShow: state.heroesToShow.filter(
          (heroe: Heroe) => heroe.id !== action.heroe.id
        )
      };
    default:
      return state;
  }
}

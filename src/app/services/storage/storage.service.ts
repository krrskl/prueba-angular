import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  constructor() {}

  /**
   *
   * @param value value to store
   */
  setItem(value) {
    return Promise.resolve().then(_ => {
      window.localStorage.setItem(environment.nameStore, JSON.stringify(value));
    });
  }

  getItem() {
    return Promise.resolve().then(_ => {
      return JSON.parse(window.localStorage.getItem(environment.nameStore));
    });
  }

  /**
   *
   * @param key Key to delete from localStorage
   */
  deleteItem(key) {
    return Promise.resolve().then(_ => {
      return window.localStorage.removeItem(key);
    });
  }
}

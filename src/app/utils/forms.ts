import { FormControl, Validators } from "@angular/forms";

export const email = () => {
  return new FormControl(
    "",
    Validators.compose([
      Validators.required,
      Validators.minLength(6),
      Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")
    ])
  );
};

export const password = () => {
  return new FormControl("", [
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(30)
  ]);
};

export const name = () => {
  return new FormControl("", Validators.required);
};

export const defaultField = () => {
  return new FormControl("", Validators.required);
}

export const fieldWithoutValidations = () => {
  return new FormControl("", null);
};


import { Component } from "@angular/core";
import { Subscription } from "rxjs";
import { User } from "./models/user.model";
import { AppState } from "./app.reducers";
import { Store } from "@ngrx/store";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { AuthService } from "./services/auth/auth.service";
import { EnableLoadingAction } from "./services/ui/ui.actions";
import { StorageService } from "./services/storage/storage.service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "prueba-angular";
  user?: User = null;
  listener: Subscription;
  public loading = null;

  constructor(
    // private authService: AuthService,
    private store: Store<AppState>,
    public router: Router,
    private ngxService: NgxUiLoaderService,
    private storageService: StorageService
  ) {
    this.store.select("auth").subscribe(data => {
      if (data.user != null) this.user = data.user;
    });

    this.store.select("ui").subscribe(ui => {
      if (ui.isLoading === true && this.loading === null) {
        this.ngxService.start();
        this.loading = true;
      } else if (this.loading) {
        this.ngxService.stop();
        this.loading = null;
      }
    });

    // this.store.dispatch(new EnableLoadingAction());
    // if (this.authService.getUser()) this.router.navigateByUrl("/home");
  }
}

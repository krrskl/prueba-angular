import { Component, OnInit, Input } from "@angular/core";
import { Heroe } from "../../models/heroe.model";
import { HeroesService } from "../../services/heroes/heroes.service";

@Component({
  selector: "app-heroe-thumbnail",
  templateUrl: "./heroe-thumbnail.component.html",
  styleUrls: ["./heroe-thumbnail.component.scss"]
})
export class HeroeThumbnailComponent implements OnInit {
  @Input("heroe") heroe: Heroe;

  constructor(private heroeService: HeroesService) {}

  ngOnInit() {}

  openModal() {
    this.heroeService.setHeroeToDetails(this.heroe);
  }
}

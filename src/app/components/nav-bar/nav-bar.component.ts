import { Component, OnInit } from "@angular/core";
import { User } from "../../models/user.model";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducers";

@Component({
  selector: "app-nav-bar",
  templateUrl: "./nav-bar.component.html",
  styleUrls: ["./nav-bar.component.scss"]
})
export class NavBarComponent implements OnInit {
  public user?: User = null;
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.select("auth").subscribe(({ user }) => {
      if (user) this.user = user;
    });
  }
}

import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducers";
import { Heroe } from "../../models/heroe.model";
import { ToggleModalAction } from "./../../services/ui/ui.actions";

@Component({
  selector: "app-modal-details",
  templateUrl: "./modal-details.component.html",
  styleUrls: ["./modal-details.component.scss"]
})
export class ModalDetailsComponent implements OnInit {
  public show: boolean = false;
  public heroe: Heroe = null;
  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.store.select("ui").subscribe(({ showModal }) => {
      this.show = showModal;
    });

    this.store.select("heroes").subscribe(({ heroe }) => {
      if (heroe) this.heroe = heroe;
    });
  }

  close() {
    this.store.dispatch(new ToggleModalAction());
  }
}

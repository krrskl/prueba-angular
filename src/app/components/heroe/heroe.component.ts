import { Component, OnInit, Input } from "@angular/core";
import { Heroe } from "../../models/heroe.model";
import { AuthService } from "../../services/auth/auth.service";
import { HeroesService } from "../../services/heroes/heroes.service";

@Component({
  selector: "app-heroe",
  templateUrl: "./heroe.component.html",
  styleUrls: ["./heroe.component.scss"]
})
export class HeroeComponent implements OnInit {
  @Input("heroe") heroe: Heroe;
  @Input("showDelete") showDelete: boolean = false;
  constructor(
    private authService: AuthService,
    private heroeService: HeroesService
  ) {}

  ngOnInit() {}

  addToFavorite() {
    if (!this.showDelete) this.authService.addFavorite(this.heroe);
    else this.heroeService.addHeroeToShow(this.heroe);
  }

  removeOfFavorite() {
    this.authService.removeFavorite(this.heroe);
  }

  remove() {
    this.heroeService.removeHeroe(this.heroe);
  }
}

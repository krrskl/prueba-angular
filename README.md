# Prueba Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.1.

___
## Requisitos
Es importante tener instalado el [Angular CLI](https://github.com/angular/angular-cli)
## Configuración inicial
1. Clonar el repositorio `git clone https://bitbucket.org/krrskl/prueba-angular.git PruebaFrontRuben`

2. cd `path/to/project/PruebaFrontRuben`

3. Usar el comando `git checkout release/development` para moverse a la rama de desarrollo

4. Instalar las dependencias de desarrollo
```bash
npm i          # Usando NPM

yarn install   # Usando YARN
```

## Corriendo el servidor
El siguiente comando nos ejecutará el servidor y automáticamente abrirá el navegador con la url en la que se está ejecutando dicho proyecto.
```sh
ng serve -o
```

___
Hecho con ❤️ por [Rubén Carrascal](https://krrskl.github.io/)
